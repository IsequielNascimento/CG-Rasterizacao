import math
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.image as mpimg

class FiguraR2:

  def __init__(self):
    self.listaPontos = []
    self.listaPontosNomes = []
    self.listaArestas = []
    self.listaArestasNomes = []
    self.listaFaces = []
    self.listaFacesNomes = []

  def addPonto(self, x, y, rotulo):
    if ([x,y] not in self.listaPontos) and (rotulo not in self.listaPontosNomes):
      self.listaPontos.append([x, y])
      self.listaPontosNomes.append(rotulo)
    else:
      print(f"O ponto [{x},{y}] ou o nome ({rotulo}) já existem na figura.")

  def addAresta(self, p1, p2, rotulo):

    try:
      index1 = self.listaPontosNomes.index(p1)
      index2 = self.listaPontosNomes.index(p2)

      ponto1 = self.listaPontos[index1]
      ponto2 = self.listaPontos[index2]

      if ([ponto1, ponto2] not in self.listaArestas) and ([ponto2, ponto1] not in self.listaArestas) and (rotulo not in self.listaArestasNomes) and (ponto1 != ponto2):
        self.listaArestas.append([ponto1, ponto2])
        self.listaArestasNomes.append(rotulo)
      else:
        if ponto1 == ponto2:
          print(f"Os pontos {p1} e {p2} são iguais.")
        else:
          print(f"A aresta [{ponto1},{ponto2}] ou o nome ({rotulo}) já existem na figura.")

    except Exception as error:
      print(f"Não foi possível achar os pontos. Erro: {error}")

  def addFace(self, arestas, rotulo):

    face = []

    try:
      for i in arestas:
        indexAresta = self.listaArestasNomes.index(i)
        face.append(self.listaArestas[indexAresta])

      if (face not in self.listaFaces) and (rotulo not in self.listaFacesNomes) and (len(face) >= 3):
        self.listaFaces.append(face)
        self.listaFacesNomes.append(rotulo)
      else:
        if len(face) < 3:
          print(f"É necessário no mínimo 3 arestas, {len(face)} foram passadas.")
        else:
          print(f"A face {face} ou o nome ({rotulo}) já existem na figura.")

    except Exception as error:
      print(f"Não foi possível achar as arestas. Erro: {error}")

  def pontosAresta(self, aresta):
    if aresta in self.listaArestasNomes:
      indexAresta = self.listaArestasNomes.index(aresta)
      return self.listaArestas[indexAresta]
    else:
      print(f"Não existe a aresta {aresta}")

  def arestasFace(self, face):
    if face in self.listaFacesNomes:
      indexFace = self.listaFacesNomes.index(face)
      return self.listaFaces[indexFace]
    else:
      print(f"Não existe a face {face}")

teste = FiguraR2()
teste.addPonto(1,3,'c')
print("Lista de pontos: ",teste.listaPontos, teste.listaPontosNomes)
teste.addAresta('b','c', 'confia tres')
teste.listaArestas, teste.listaArestasNomes
teste.addFace(['confia um', 'confia dois', 'confia tres'], 'face confia')
print(teste.listaFaces, teste.listaFacesNomes)

def plot_image(pontos, resolucao):
  img = np.ones((resolucao[1], resolucao[0]))

  for i in pontos:

    img[int(i[1])][int(i[0])] = 0

  img = pintarFace(img, resolucao)

  fig, ax = plt.subplots(figsize=(15, 8))

  ax.imshow(img, cmap='gray', origin='lower', vmin=0, vmax=1)
  plt.show()


def pintarFace (img, resolucao):

  X = resolucao[1]
  Y = resolucao[0]

  for i in range(X):
    start = []
    for j in range(1, Y):
      if (img[i][j] == 1) and (img[i][j-1] == 0):
        start.append(j-1)

    if (img[i][-1] == 0) and (img[i][-2] == 1):
      start.append(Y-1)

    if (len(start) == 0) or (len(start) == 1):
      continue

    start.append(Y+1)
    contador = 0

    for k in range(Y):
      if k == start[contador]:
        contador+=1

      if contador%2:
        img[i][k] = 0

  return img

def produz_frag(x, y):
  xm = int(x)
  ym = int(y)

  xp = xm + 0.5
  yp = ym + 0.5

  return xp, yp

def addPonto(x,y, lista, resolucao):
  x_, y_ = produz_frag(x, y)

  if x_ >= resolucao[0]:
    x_ = resolucao[0] - 1
  if y_ >= resolucao[1]:
    y_ = resolucao[1] - 1

  if [x_, y_] not in lista:
    lista.append([x_, y_])

  return lista

def convertePontos (pontos, resolucao):
  pontosConvertidos = []

  for i in pontos:

    x = round((resolucao[0]*(i[0] + 1)) / 2, 2)
    y = round((resolucao[1]*(i[1] + 1)) / 2, 2)


    pontosConvertidos.append([x,y])

  return pontosConvertidos

def rasterizacao_retas(p1o, p2o, resolucao, passo = 1):

  p1, p2 = convertePontos([p1o, p2o], resolucao)

  listaPontos = []

  x = p1[0]
  y = p1[1]

  X = p2[0]
  Y = p2[1]

  dx = X - x
  dy = Y - y

  if dx == 0:

    if dy > 0:
      while (y < Y):

        x = x
        listaPontos = addPonto(x, y, listaPontos, resolucao)
        y += passo

    else:
      while (y > Y):

        x = x
        listaPontos = addPonto(x, y, listaPontos, resolucao)
        y -= passo

    return listaPontos

  m = dy / dx
  b = y - m*x


  if (abs(dx) > abs(dy)):
    if dx > 0:
      while(x < X):

        y = m*x + b
        listaPontos = addPonto(x, y, listaPontos, resolucao)
        x = x + passo
    else:
      while(x > X):

        y = m*x + b
        listaPontos = addPonto(x, y, listaPontos, resolucao)
        x = x - passo

  else:
    if dy > 0:
      while (y < Y):
        x = (y-b)/m
        listaPontos = addPonto(x, y, listaPontos, resolucao)
        y = y + passo
    else:
      while (y > Y):
        x = (y-b)/m
        listaPontos = addPonto(x, y, listaPontos, resolucao)
        y = y - passo

  return listaPontos

resolucao = [100,50]
listaPontos = rasterizacao_retas([0, 0.5], [0.3, 0.25], resolucao)
plot_image(listaPontos, resolucao)